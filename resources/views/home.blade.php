@extends('layouts.app')
@section('content')
    @csrf
{{--   <h1>All</h1>--}}

    <form method="GET" action="{{ url('/new/') }}"  style="margin-top: 20px;">
        <h1 style="float: left;" >SHORT URL WEBSITE</h1>
        <button class="btn btn-danger" style="float: right;" > New SHORT URL</button>
    </form>
{{--    <form method="GET" action="{{ url('/') }}" style="margin-left: 30px;">--}}
{{--        <button class="btn btn-danger" > View all post</button>--}}
{{--    </form>--}}
    <br>

    <table class="table">
        <thead class="bg-primary" style="color: white;">
        <tr>
            <th scope="col">Date</th>
            <th scope="col">Longurl</th>
            <th scope="col">Shorturl</th>
            <th scope="col">view</th>
            <th scope="col">copy</th>
        </tr>
        </thead>
    @if(count($shorts)>0)

        @foreach($shorts as $short)

                <tbody>
                <tr>
                    <td>  <p>{{ $short->created_at }}</p></td>
                    <td >
                        <a href="{{$short->longurl}}">
                            <h3>{{ $short->longurl }}</h3>
                        </a>
                    </td>
                    <td> <input id ="short{{$short->id}}"  type="text" value="http://www.short.local/t/{{ $short->shorturl }}" readonly></td>
                    <td> <p>{{ $short->view }}</p></td>
                    <td>  <p><button onclick="copy(this)" value="{{$short->id}}" type="button">
                                Copy
                            </button></p></td>
                </tr>
                </tbody>
            @endforeach
        @endif
    </table>


    {{--            <div>--}}
    {{--               <a href="{{$short->longurl}}">--}}
    {{--                    <h3>{{ $short->longurl }}</h3>--}}
    {{--                </a>--}}

    {{--               <input id ="short{{$short->id}}"  type="text" value="http://www.short.local/t/{{ $short->shorturl }}" readonly>--}}
    {{--                <button onclick="copy(this)" value="{{$short->id}}" type="button">--}}
    {{--                    Copy--}}
    {{--                </button>--}}
    {{--                <p>{{ $short->view }}</p>--}}
    {{--                <p>{{ $short->created_at }}</p>--}}

    {{--            </div>--}}




    <script>
        function copy(text) {
            var id  = text.value;
            var copyText = document.querySelector('#short'+id);
            copyText.select();
            document.execCommand('copy');
            alert('All :'+copyText.value);

        }
    </script>



@endsection
