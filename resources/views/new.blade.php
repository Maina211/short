@extends('layouts.app')
@section('content')
    <h1 class="text-primary" style="text-align: center;margin-top: 100px;">LONG URL</h1>
<div class="border border-primary" style="border-width: 3px">
    <br><br>
    <form method="post"  action="{{ url('/') }}" enctype="multipart/form-data">
        @csrf
        <div class="container " >
            <div class="row justify-content-md-center">
                <div class="form-group col-12" style="text-align: center;">
                    <input type="text" name="longurl" class="form-control">
                </div>

                <button type="submit" class="btn btn-primary col-3">CREATE SHORT URL</button>
            </div>
        </div>

    </form>
    <br><br>
</div>

    <form method="GET" action="{{ url('/home/') }}">

        <button class="btn btn-secondary col-2" style="float: left;margin-top: 50px;" >back to home</button>

    </form>




@endsection

