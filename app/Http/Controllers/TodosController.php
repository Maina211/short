<?php

namespace App\Http\Controllers;

use App\short;
use App\Todo;
use Illuminate\Http\Request;

class TodosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $shorts = short::OrderBy('created_at','desc')->get();
        return view('home')->with('shorts',$shorts);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return  view('new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $charEng = "abcdefghijklmnopgrstuvwxyz";
        $num09 = "0123456789";
        $url ="";
        for($i = 0 ; $i<5; $i++){
            $url .= $num09[rand(0,strlen($num09))-1];
        }
        $url .= $charEng[rand(0,strlen($charEng))-1];
        $this->validate($request,
        [
            'longurl' =>'required',
        ]
        );
        $todo = new short();
        $todo->longurl = $request->input('longurl');
        $todo->shorturl = $url;
        $todo->view = 0;
        $todo->save();
        //return redirect('/new')->with('success','Success!');
        return redirect('/new')->with('success','Success short! : '.'www.short.local/'.$url);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $todos = short::all();
        if(count($todos)>0){
            foreach ($todos as $todo){
                if($todo->shorturl == $id){
                    $todo->view +=1;
                    $todo->save();
                    return view('show')->with('longurl',$todo->longurl);
                }
            }
        }
        return view('notShow');
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
